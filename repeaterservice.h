#ifndef REPEATERSERVICE_H
#define REPEATERSERVICE_H

#include "qtservice.h"
#include "repeaterdaemon.h"

class RepeaterService : public QtService<QCoreApplication>
{
public:
    RepeaterService(int argc, char **argv);

protected:
    void start();
    void pause();
    void resume();

private:
    RepeaterDaemon *daemon;
};

#endif // REPEATERSERVICE_H
