#ifndef REPEATERDAEMON_H
#define REPEATERDAEMON_H

#include <QTcpServer>
#include <QTcpSocket>
#include "qtservice.h"

class RepeaterDaemon : public QTcpServer
{
    Q_OBJECT
public:
    RepeaterDaemon(quint16 port, QObject* parent = 0);

    void pause();
    void resume();
protected:
    void incomingConnection(qintptr socket);

private slots:
    void readClient();
    void discardClient();
private:
    bool disabled;
};

#endif // REPEATERDAEMON_H
